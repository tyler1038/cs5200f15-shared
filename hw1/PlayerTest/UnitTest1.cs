﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Messages;
using SharedObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Player;

namespace PlayerTest
{
    ///////////////////////////////////////////////////////////////
    //          Doesn't work....
    //
    //////////////////////////////////////////////////////////////
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void LoginRequestTest()
        {
            // Create local updclient
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 12500);
            IPEndPoint registryEndPoint = new IPEndPoint(IPAddress.Any, 12550);
            UdpClient localClient = new UdpClient(localEndPoint);
            UdpClient registryClient = new UdpClient(registryEndPoint);
            
            IdentityInfo player = new IdentityInfo(){Alias = "Tyler1038", ANumber = "A01561419", FirstName = "Tyler", LastName = "Knighton"};

            Thread listenThread = new Thread(() => this.listenThread(registryEndPoint, registryClient));
            listenThread.Start();

            ProcessInfo info = Program.LoginRequest(registryEndPoint, player, localClient);
            
            Assert.AreEqual(info.ProcessId, 1250);

            // Create another udpclient

            // start a thread to list on the second client
            // call the login function
        }

        public void listenThread(IPEndPoint localEndPoint, UdpClient regClient)
        {            
            byte[] bytes = regClient.Receive(ref localEndPoint);
            Message m1 = Message.Decode(bytes);
            if (m1 is LoginRequest)
            {
                LoginRequest recievedRequest = m1 as LoginRequest;

                Assert.AreEqual(recievedRequest.Identity.ANumber, "A01561419" );
                Assert.AreEqual(recievedRequest.Identity.Alias, "Tyler1038");
                Assert.AreEqual(recievedRequest.Identity.FirstName, "Tyler");
                Assert.AreEqual(recievedRequest.Identity.LastName, "Knighton");
                Assert.AreEqual(recievedRequest.ProcessType, ProcessInfo.ProcessType.Player);
                Assert.AreEqual(recievedRequest.ProcessLabel, "Tyler1038");

                ProcessInfo newInfo = new ProcessInfo() { ProcessId = 1250 };
                LoginReply reply = new LoginReply(){ProcessInfo = newInfo, Success = true, Note = "testing..."};
                byte[] replyBytes = reply.Encode();

                regClient.SendAsync(replyBytes, replyBytes.Length, localEndPoint);
            }
            else
            {
                Assert.Fail("Didn't receive LoginRequest");
            }
            

        }
    }
}
