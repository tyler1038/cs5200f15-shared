﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using Messages;
using SharedObjects;

namespace Player
{
    public class Program
    {
        static void Main(string[] args)
        {
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 0);
            UdpClient registryClient = new UdpClient(localEndPoint);
            IPEndPoint registryEndPoint = (new PublicEndPoint("52.3.213.61:12000")).IPEndPoint;
            IdentityInfo player = new IdentityInfo(){Alias = "Tyler1038", ANumber = "A01561419", FirstName = "Tyler", LastName = "Knighton"};
            ProcessInfo playerProcessInfo;
            List<GameInfo> listGameInfo; 
            JoinGameReply currentGameReply = new JoinGameReply();
            
            //Login Request
            playerProcessInfo = LoginRequest(registryEndPoint, player, registryClient);

            if (playerProcessInfo != null)
            {
                //Getting game list
                listGameInfo = GameList(registryEndPoint, registryClient);

                foreach (GameInfo t in listGameInfo.Where(t => t.Status == GameInfo.StatusCode.Available))
                {
                    //Connect to Game
                    if (ConnectGame(t, playerProcessInfo, registryClient, ref currentGameReply))
                    {
                        break;
                    }
                }
                Console.WriteLine("Initial Life Points: {0}.", currentGameReply.InitialLifePoints);

                //Start Alive Request Thread
                Thread aliveRequestThread = new Thread(() => AliveRequest(registryEndPoint, registryClient))
                {
                    IsBackground = true
                };
                aliveRequestThread.Start();
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Following Code used from internet: https://msdn.microsoft.com/en-us/library/system.console.keyavailable.aspx
            // In order to have logout without threads being stopped waiting on user input.

            ConsoleKeyInfo cki;

            do
            {
                Console.WriteLine("\npress the 'x' key to quit.");

                while (Console.KeyAvailable == false)
                {
                    Thread.Sleep(1000);
                }
                cki = Console.ReadKey(true);
                Console.WriteLine("You pressed the '{0}' key.", cki.Key);
            } while (cki.Key != ConsoleKey.X);

            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            LogoutRequest playerLogoutRequest = new LogoutRequest();
            registryClient.SendAsync(playerLogoutRequest.Encode(), playerLogoutRequest.Encode().Length, registryEndPoint);
            

            
        }

        private static bool ConnectGame(GameInfo t, ProcessInfo player,  UdpClient registryClient, ref JoinGameReply joinGameReply)
        {           
            bool success = false;            
            IPEndPoint GMEndPoint = t.GameManager.EndPoint.IPEndPoint;
            JoinGameRequest playerJoinGameRequest = new JoinGameRequest(){ GameId = t.GameId, Player = player};
            byte[] requestBytes = playerJoinGameRequest.Encode();
            //send Request
            registryClient.SendAsync(requestBytes, requestBytes.Length, GMEndPoint);

            //Recieve reply
            Console.WriteLine("Waiting for reply..");
            byte[] bytes = registryClient.Receive(ref  GMEndPoint);
            Message replyMessage = Message.Decode(bytes);

            //If correct reply set ref variable and success
            if (replyMessage is JoinGameReply)
            {
                Console.WriteLine("Connect to game was succesful.\n");
                joinGameReply = replyMessage as JoinGameReply;
                success = true;
            }
            return success;
        }

        public static ProcessInfo LoginRequest(IPEndPoint registryEndPoint, IdentityInfo player, UdpClient registryClient)
        {
            LoginRequest playerLoginRequest = new LoginRequest(){Identity = player, ProcessType = ProcessInfo.ProcessType.Player, ProcessLabel = player.Alias};
            byte[] requestBytes = playerLoginRequest.Encode();
            // Send Request
            registryClient.SendAsync(requestBytes, requestBytes.Length, registryEndPoint);
            
            //Recieve Reply
            Console.WriteLine("Waiting for reply..");
            byte[] bytes = registryClient.Receive(ref registryEndPoint);
            Message replyMessage = Message.Decode(bytes);
            LoginReply replyLoginReply = replyMessage as LoginReply;

            //if Correct message return ProcessInfo
            if (!(replyLoginReply == null || !replyLoginReply.Success))
            {
                Console.WriteLine("Succesful login attempt.\n");
                return replyLoginReply.ProcessInfo;
            }
            else
            {
                Console.WriteLine("Unsucceseful Login attempt.\n");
                return null;
            }
            
        }

        private static void AliveRequest(IPEndPoint registryEndPoint, UdpClient registryClient)
        {
            while (true)
            {
                //Recieve Alive 
                byte[] bytes = registryClient.Receive(ref registryEndPoint);
                Message m1 = Message.Decode(bytes);

                //if correct message send reply
                if (m1 is AliveRequest)
                {
                    Console.WriteLine("sending AliveReply..");
                    Reply aliveReply = new Reply();
                    byte[] replyBytes = aliveReply.Encode();
                    registryClient.SendAsync(replyBytes, replyBytes.Length, registryEndPoint);
                }
                Thread.Sleep(1000);
            }
        }

        private static List<GameInfo> GameList(IPEndPoint registryEndPoint, UdpClient registryClient)
        {
            bool success = false;
            List<GameInfo> list = new List<GameInfo>();

            //loop to keep getting a list
            while (!success)
            {
                GameListRequest listRequest = new GameListRequest() {StatusFilter = -1};
                byte[] requestBytes = listRequest.Encode();
                //send Request
                registryClient.SendAsync(requestBytes, requestBytes.Length, registryEndPoint);

                //Recieve Reply
                Console.WriteLine("Waiting for reply...");
                byte[] bytes = registryClient.Receive(ref registryEndPoint);
                Message m1 = Message.Decode(bytes);

                //if correct add gameinfo to list and return
                if (m1 is GameListReply)
                {
                    Console.WriteLine("Game List fetch successeful\n");
                    GameListReply listReply = m1 as GameListReply;
                    if (listReply != null && listReply.Success)
                    {
                        foreach (GameInfo t in listReply.GameInfo)
                        {
                           // Console.WriteLine("{0}\n", t.Status);
                            list.Add(t);
                        }
                        success = true;
                    }
                    else
                    {
                        throw new Exception("Incorrect gamelist reply\n");
                    }
                    
                }
                else
                {
                    throw new Exception("Incorrect gamelist reply");
                }
            }
            return list;
        }
    
    }
}
