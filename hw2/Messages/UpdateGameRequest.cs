﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages
{
    [DataContract]
    public class UpdateGameRequest : Request
    {
        [DataMember]
        public GameInfo Game { get; set; }
    }
}
